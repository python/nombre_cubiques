# -*- coding: utf-8 -*-

import time

print("/!\ le temps d'éxecution au-delà de 1 000 000 est très long /!\ \n")

while True :
    k=int(input("Puissance : "))
    (init,n)=(int(input("Analyser les nombres de : ")),int(input("à : ")))
    print("\n")
    
    ti=time.time()
    for i in range(init,n+1) :
        l=[int(c) for c in str(i)]
        s=0
        for j in range(0,len(l)) : s+=l[j]**k
        if s==i : print(i)
    tf=time.time()
    
    print("\n")
    print("Programme exécuté en %s s"%round(tf-ti,2))
    print("---------------------------")
